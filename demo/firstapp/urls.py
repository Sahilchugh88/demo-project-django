from django.urls import path
from django.urls.resolvers import URLPattern
from .views import index
app_name = 'firstapp'

urlpatterns = [
    path('api/', view=index, name='indexfunction')
]
